//
//  UploadWeather.m
//  WeatherObjc
//
//  Created by Анастасия Исаева on 02.06.17.
//  Copyright © 2017 Анастасия Исаева. All rights reserved.
//

#import "DownloadWeather.h"
#import "RealmWorker.h"

@implementation DownloadWeather{
    AFURLSessionManager *manager;
}

-( id ) init
{
    self = [ super init ];
    
    if ( nil != self )
    {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    }
    
    return self;
}

-(void)downloadWeather:(double) latitude longitude:(double)longitude completion:(void (^)(Weather *weather))completion{
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&units=metric&APPID=00eade5cfeee80ce144af9bc108b7628", latitude, longitude]];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *task = [manager dataTaskWithRequest:request
               completionHandler:^(NSURLResponse *response, id responseObject, NSError *error){
                   if (error != nil){
                       dispatch_async(dispatch_get_main_queue(), ^{
                           UIAlertView *alert = [[UIAlertView new] initWithTitle:@"" message:[NSString stringWithFormat:@"При соединении с сервером произошла ошибка %@. Попробуйте через некоторое время обновить прогноз погоды", [error localizedDescription]] delegate:self cancelButtonTitle:@"Ок" otherButtonTitles:nil];
                           [alert show];
                       });
                   } else {
                       NSDictionary *dict = (NSDictionary *)responseObject;
                       if (dict != nil){
                         Weather *weather = [Weather new];
                         [weather parser:dict];
                           
                         RealmWorker *rw = [RealmWorker new];
                         [rw updateWeather:weather];
                           
                         completion(weather);
                       }
                   }
               }];
    [task resume];
}


-(void)downloadImageWeather:(NSString *)iconName completion:(void (^)(NSString *file))completion{
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://openweathermap.org/img/w/%@", iconName]];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
        
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        completion(filePath.path);
    }];
    [downloadTask resume];
}

@end
