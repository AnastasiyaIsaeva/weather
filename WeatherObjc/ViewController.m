//
//  ViewController.m
//  WeatherObjc
//
//  Created by Анастасия Исаева on 02.06.17.
//  Copyright © 2017 Анастасия Исаева. All rights reserved.
//

#import "ViewController.h"
#import "RealmWorker.h"

@implementation ViewController
{
    LocationUtils *locationUtils;
    DownloadWeather *downloadWeather;
    BOOL isWeatherDownload;
    
    NSTimer *timer;
    
    int counter;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    locationUtils = [LocationUtils new];
    downloadWeather = [DownloadWeather new];
    locationUtils.delegate = self;

    counter = 0;
    
    [_progressIndicator setHidden:YES];
    _cityLabel.text = @"Определение местоположения...";
    
    timer = [NSTimer scheduledTimerWithTimeInterval:600 target:self selector:@selector(updateWeather) userInfo:nil repeats:YES];
    
    RealmWorker *rw = [RealmWorker new];
    Weather * w = [rw getWeather];
    if (w != nil){
        int delta = [[NSDate new] timeIntervalSinceNow] - w.lastUpdate;
        if (delta > 600){
            [self updateWeather];
        } else {
            [self updateDisplay:w];
        }
    } else {
        double lat = [[[NSUserDefaults standardUserDefaults] valueForKey:@"lat"] doubleValue];
        double lon = [[[NSUserDefaults standardUserDefaults] valueForKey:@"lon"] doubleValue];
        if (!(lat == 0 && lon == 0)){
            [self updateWeather];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [locationUtils startMonitoring];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage *)rotate:(UIImage *)image radians:(float)rads
{
    float newSide = MAX([image size].width, [image size].height);
    CGSize size =  CGSizeMake(newSide, newSide);
    UIGraphicsBeginImageContext(size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(ctx, newSide/2, newSide/2);
    CGContextRotateCTM(ctx, rads);
    CGContextDrawImage(UIGraphicsGetCurrentContext(),CGRectMake(-[image size].width/2,-[image size].height/2,size.width, size.height),image.CGImage);
    
    UIImage *i = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return i;
}


-(void)updateImage:(Weather *)weather{
    
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, NO) firstObject];
    
    NSString *ic = [NSString stringWithFormat:@"%@/%@.png", path, weather.imageFile];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:ic])
      [downloadWeather downloadImageWeather:[weather.imageFile stringByAppendingString:@".png"] completion:^(NSString *path){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateDisplay:weather];
        });
     }];
    else
        [self updateDisplay:weather];
}

-(void)updateDisplay:(Weather *)weather{
    isWeatherDownload = YES;
    [_progressIndicator stopAnimating];
    [_progressIndicator setHidden:YES];
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    
    UIImage *image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.png", path, weather.imageFile]];
    
    _imageWeather.image = image;
    _tempLabel.text = [NSString stringWithFormat:@"%0.0f˚C", weather.temp];
    _cityLabel.text = weather.city;
    _windLabel.text = [NSString stringWithFormat:@"%0.0f м/c", weather.windSpeed];
    _directionView.image = [self rotate:[UIImage imageNamed:@"arrow.png"] radians:weather.windDegree*M_PI/180.0];
}


-(void)updateWeather{
    [_progressIndicator startAnimating];
    [_progressIndicator setHidden:NO];
    double lat = [[[NSUserDefaults standardUserDefaults] valueForKey:@"latitude"] doubleValue];
    double lon = [[[NSUserDefaults standardUserDefaults] valueForKey:@"longitude"] doubleValue];
    
    [downloadWeather downloadWeather:lat longitude:lon completion:^(Weather *weather){
            dispatch_async(dispatch_get_main_queue(), ^{
                if (weather != nil)
                  [self updateImage:weather];
                else
                {
                    UIAlertView *alert = [[UIAlertView new] initWithTitle:@"" message:@"При получении погоды произошла неизвестная ошибка. Попробуйте обновить прогноз погоды" delegate:self cancelButtonTitle:@"Ок" otherButtonTitles:nil];
                    [alert show];
                    [_progressIndicator stopAnimating];
                    [_progressIndicator setHidden:YES];
                }
            });
        
     }];
}

- (IBAction)clickRefresh:(id)sender {
    [self updateWeather];
}

-(void)updatedLocation{
    if (!isWeatherDownload){
        [self updateWeather];
    }
}


@end
