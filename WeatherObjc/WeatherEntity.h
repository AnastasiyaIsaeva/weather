//
//  WeatherEntity.h
//  WeatherObjc
//
//  Created by Анастасия Исаева on 03.06.17.
//  Copyright © 2017 Анастасия Исаева. All rights reserved.
//

#import <Realm/Realm.h>

@interface WeatherEntity : RLMObject

@property NSInteger id;
@property NSString *city;
@property NSString *imageFile;
@property double temp;
@property double windSpeed;
@property double windDegree;
@property long lastUpdate;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<WeatherEntity *><WeatherEntity>
RLM_ARRAY_TYPE(WeatherEntity)
