//
//  WeatherEntity.m
//  WeatherObjc
//
//  Created by Анастасия Исаева on 03.06.17.
//  Copyright © 2017 Анастасия Исаева. All rights reserved.
//

#import "WeatherEntity.h"

@implementation WeatherEntity

+ (NSString *)primaryKey {
    return @"id";
}


// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

@end
