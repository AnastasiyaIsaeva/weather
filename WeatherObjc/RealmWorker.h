//
//  RealmWorker.h
//  WeatherObjc
//
//  Created by Анастасия Исаева on 03.06.17.
//  Copyright © 2017 Анастасия Исаева. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Weather.h"

@interface RealmWorker : NSObject

-(void)updateWeather:(Weather *)weather;

-(Weather *)getWeather;

@end
