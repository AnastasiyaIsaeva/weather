//
//  LocationUtils.m
//  WeatherObjc
//
//  Created by Анастасия Исаева on 02.06.17.
//  Copyright © 2017 Анастасия Исаева. All rights reserved.
//

#import "LocationUtils.h"

@implementation LocationUtils

int mode;

-( id ) init
{
    self = [ super init ];
    
    if ( nil != self )
    {
        self.locationManager = [CLLocationManager new];
    }
    
    return self;
}

-(void)dealloc{
    [self.locationManager stopUpdatingLocation];
}

-(BOOL)answerAlert{
    if ([CLLocationManager locationServicesEnabled]){
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined){
           [self.locationManager requestWhenInUseAuthorization];
            return false;
        } else
            return true;
    } else {
        UIAlertView *alert = [[UIAlertView new] initWithTitle:@"" message:@"Для определения города необходимо включить сервисы геолокации. Включить сейчас?" delegate:self cancelButtonTitle:@"Нет" otherButtonTitles:@"Да", nil];
        [alert show];
        mode = 0;
    }
    return false;
}

-(void)startMonitoring{
    if ([self answerAlert]){
        [self setSettingsLocationManager];
    }
}

-(void)setSettingsLocationManager{
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    [self.locationManager startUpdatingLocation];
}


#pragma CCLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations{
    CLLocation *loc = [locations lastObject];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithDouble:loc.coordinate.latitude] forKey:@"latitude"];
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithDouble:loc.coordinate.longitude] forKey:@"longitude"];
    
    if (self.delegate)
      [self.delegate updatedLocation];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    [self startMonitoring];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    NSLog(@"%@", error);
}

#pragma UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (mode){
        case 0:{
      switch (buttonIndex){
        case 0:{
            UIAlertView *alert = [[UIAlertView new] initWithTitle:@"" message:@"Невозможно определить местоположение. Приложение будет завершено" delegate:self cancelButtonTitle:@"Ок" otherButtonTitles:nil];
            [alert show];
        }
            break;
        case 1:
            if ([[UIDevice currentDevice].systemVersion intValue] < 10)
               [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=LOCATION_SERVICES"]];
              else
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-prefs:root=LOCATION_SERVICES"]];  
            break;
     }
        }
            break;
        case 1:
            //завершение приложения
            break;
    }
}


@end
