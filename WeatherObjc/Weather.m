//
//  Weather.m
//  WeatherObjc
//
//  Created by Анастасия Исаева on 02.06.17.
//  Copyright © 2017 Анастасия Исаева. All rights reserved.
//

#import "Weather.h"

@implementation Weather
{
    NSString *_city;
    NSString *_imageFile;
    double temp;
    double windSpeed;
    double windDegree;
}

@synthesize city = _city;
@synthesize imageFile = _imageFile;
@synthesize temp = _temp;
@synthesize windSpeed = _windSpeed;
@synthesize windDegree = _windDegree;

-(void)parser:(NSDictionary *)json{
    
    NSString *name = [json valueForKey:@"name"];
    self.city = name;
    
    NSArray *weatherArr = [json valueForKey:@"weather"];
    if (weatherArr != nil && [weatherArr count] > 0){
        NSDictionary *weatherDict = [weatherArr firstObject];
        self.imageFile = [weatherDict valueForKey:@"icon"];
    }
    
    NSDictionary *weatherDict = [json valueForKey:@"main"];
    if (weatherDict != nil){
        NSNumber *num = [weatherDict valueForKey:@"temp"];
        if (num != nil){
           self.temp = [num doubleValue];
        }
    }
    
    weatherDict = [json valueForKey:@"wind"];
    if (weatherDict != nil){
        NSNumber *num = [weatherDict valueForKey:@"speed"];
        if (num != nil){
            self.windSpeed = [num doubleValue];
        }
        
        num = [weatherDict valueForKey:@"deg"];
        if (num != nil){
            self.windDegree = [num doubleValue];
        }
    }
    
    self.lastUpdate = [[json valueForKey:@"dt"] longValue];
}
  

@end
