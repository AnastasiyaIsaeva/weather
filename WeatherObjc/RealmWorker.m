//
//  RealmWorker.m
//  WeatherObjc
//
//  Created by Анастасия Исаева on 03.06.17.
//  Copyright © 2017 Анастасия Исаева. All rights reserved.
//

#import "RealmWorker.h"
#import "WeatherEntity.h"

@implementation RealmWorker


-(void)updateWeather:(Weather *)weather{
    WeatherEntity *we = [[WeatherEntity alloc] init];
    we.city = weather.city;
    we.windSpeed = weather.windSpeed;
    we.windDegree = weather.windDegree;
    we.imageFile = weather.imageFile;
    we.temp = weather.temp;
    we.lastUpdate = weather.lastUpdate;
    we.id = 1;
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm addOrUpdateObject:we];
    [realm commitWriteTransaction];

}

-(Weather *)getWeather{
    WeatherEntity *we = [[WeatherEntity objectsWhere:@"id == 1"] firstObject];
    Weather *weather = [Weather new];
    weather.city = we.city;
    weather.windSpeed = we.windSpeed;
    weather.windDegree = we.windDegree;
    weather.imageFile = we.imageFile;
    weather.temp = we.temp;
    weather.lastUpdate = we.lastUpdate;
    return weather;
}

@end
