//
//  LocationUtils.h
//  WeatherObjc
//
//  Created by Анастасия Исаева on 02.06.17.
//  Copyright © 2017 Анастасия Исаева. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@protocol LocationUtilsProtocol

-(void)updatedLocation;

@end

@interface LocationUtils : NSObject <CLLocationManagerDelegate, UIAlertViewDelegate>

@property (nonatomic, strong)CLLocationManager* locationManager;
@property (nonatomic, strong)id <LocationUtilsProtocol> delegate;

-(void)startMonitoring;

@end
