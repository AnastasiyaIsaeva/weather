//
//  Weather.h
//  WeatherObjc
//
//  Created by Анастасия Исаева on 02.06.17.
//  Copyright © 2017 Анастасия Исаева. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Weather : NSObject

@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy)NSString *imageFile;
@property (nonatomic)long lastUpdate;
@property (nonatomic)double temp;
@property (nonatomic)double windSpeed;
@property (nonatomic)double windDegree;

-(void)parser:(NSDictionary *)json;

@end
