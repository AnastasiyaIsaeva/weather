//
//  ViewController.h
//  WeatherObjc
//
//  Created by Анастасия Исаева on 02.06.17.
//  Copyright © 2017 Анастасия Исаева. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationUtils.h"
#import "DownloadWeather.h"

@interface ViewController: UIViewController <LocationUtilsProtocol>
@property (weak, nonatomic) IBOutlet UIImageView *imageWeather;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *windLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *refreshButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *directionView;


@end

