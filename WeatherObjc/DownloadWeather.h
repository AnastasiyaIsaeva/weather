//
//  UploadWeather.h
//  WeatherObjc
//
//  Created by Анастасия Исаева on 02.06.17.
//  Copyright © 2017 Анастасия Исаева. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import "Weather.h"

@interface DownloadWeather : NSObject


-(void)downloadWeather:(double) latitude longitude:(double)longitude completion:(void (^)(Weather *weather))completion;
-(void)downloadImageWeather:(NSString *)iconName completion:(void (^)(NSString *file))completion;

@end
